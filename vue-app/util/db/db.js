import cache from '@/util/cache.js'
import store from '@/store/index.js'
var chatfix = 'list_chat_';
var Msgfix = 'list_msg_';
export const getChats = () => {
	return new Promise((resolve, reject) => {
		let chatList = cache.get(chatfix + store.state.user.operId)
		if (chatList) {			
			resolve(chatList)
		} else {
			reject(null)
		}
	});
}

export const getMsgList = (chatObj) => {
	return new Promise((resolve, reject) => {
		let msgList = cache.get(Msgfix + store.state.user.operId + '_' + chatObj.chatId)
		resolve(msgList)
	});
}

export const removeMsgList = (chat) => {
	let msgListName = cache.get(Msgfix + store.state.user.operId + '_' + chat.chatId)
	cache.remove(msgListName)
}

// export const getMsgList = (chatObj) => {
// 	return new Promise((resolve, reject) => {
// 		if (chatObj.chatType == 1) {
// 			let msgList = cache.get(Msgfix + chatObj.chatId)
// 			resolve(msgList)
// 		} else {
// 			let msgList = cache.get(Msgfix + chatObj.chatId)
// 			let msgListMyself = cache.get(Msgfix + store.state.user.operId)
// 			if (msgListMyself) {
// 				if (msgList) {
// 					msgListMyself.push(...msgList)
// 				}
// 				for (var i in msgListMyself) {
// 					if (msgListMyself[i].fromUserId == store.state.user.operId) {
// 						msgListMyself[i].isItMe = true
// 					}
// 				}
// 				resolve(msgListMyself)
// 			}
// 		}
// 	});
// }

export const openChat = (chat) => {
	return new Promise((resolve, reject) => {
		const cacheChatName = chatfix + store.state.user.operId
		let chatList = cache.get(cacheChatName);
		if (chatList) {
			for (var i in chatList) {
				if (chatList[i].chatId == chat.chatId) {
					reduceUserUnreadNumberTotal(chatList[i])
					chatList[i].unreadNumber = 0
					chatList[i].lastOpenTime = new Date()
					chatList[i].isOpen = true
					chatList[i].weakPrompt = false // 弱提醒
					continue
				}
			}
			cache.set(cacheChatName, chatList)
			store.commit("setConversation", chatList);
		}
	});
}

export const closeChat = (chat) => {
	return new Promise((resolve, reject) => {
		const cacheChatName = chatfix + store.state.user.operId
		let chatList = cache.get(cacheChatName);
		if (chatList) {
			for (var i in chatList) {
				if (chatList[i].chatId == chat.chatId) {
					chatList[i].unreadNumber = 0
					chatList[i].isOpen = false
					chatList[i].lastCloseTime = new Date()
					continue
				}
			}
			cache.set(cacheChatName, chatList)
			store.commit("setConversation", chatList);
		}
	});
}


function addMsg(chat) {
	let msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.chatId;
	let MsgList = cache.get(msgListCacheName)
	if (!MsgList) {
		let MsgList2 = [];
		MsgList2.push(chat);
		cache.set(msgListCacheName, MsgList2);
		return;
	}

	let pushTag = true
	for (let i in MsgList) {
		if (MsgList[i].hasBeenSentId == chat.hasBeenSentId) {
			MsgList[i].content = chat.content
			if (chat.contentType != 5) pushTag = false
			continue
		}
	}
	

	if (pushTag) {
		if (MsgList.length >= 10) {
			MsgList.splice(0, 1);
		}
		MsgList.push(chat)
	}

	cache.set(msgListCacheName, MsgList)
}


export const modifyPacket = (chat) => {
	let msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.chatId;
	let msglist = cache.get(msgListCacheName);
	if (!msglist) {
		let item = []
		item.push(chat)
		cache.set(msgListCacheName, item)
		return;
	}
	for (var i in msglist) {
		if (msglist[i].hasBeenSentId == chat.hasBeenSentId) {
			msglist[i].content = chat.content
		}
	}
	cache.set(msgListCacheName, msglist)
}

export const modifyChat = (chat) => {
	const cacheChatListName = chatfix + store.state.user.operId
	let chatList = cache.get(cacheChatListName)
	if (!chatList) return;
	for (var i in chatList) {
		if (chatList[i].chatId == chat.chatId) {
			chatList[i] = chat
		}
	}
	cache.set(cacheChatListName, chatList)
	store.commit("setConversation", chatList);
}

export const removeChat = (chat) => {
	const cacheChatListName = chatfix + store.state.user.operId
	let chatList = cache.get(cacheChatListName)
	if (!chatList) return;
	let idx = -1;
	for (var i in chatList) {
		if (chatList[i].chatId == chat.chatId) {
			idx = i
			break
		}
	}
	if (idx != -1) chatList.splice(idx, 1);
	cache.set(cacheChatListName, chatList)
	store.commit("setConversation", chatList);
}

export const addChat = (chat) => {
	return new Promise((resolve, reject) => {
		if (chat.command == 3 || chat.command == 4) {

			if (chat.content == "") {
				return;
			}

			const cacheChatName = chatfix + store.state.user.operId

			chat.unreadNumber = chat.fromUserId == store.state.user.operId ? 0 : 1 // 当前登陆人发送的或者会话被打开不计次数
			chat.lastOpenTime = Date.now()
			chat.stayOnTop = Boolean(chat.stayOnTop) // 保持在顶部
			chat.doNotDisturb = Boolean(chat.doNotDisturb) // 消息免打扰
			chat.isOpen = Boolean(chat.isOpen) // 会话打开状态
			chat.weakPrompt = !Boolean(chat.fromUserId == store.state.user.operId) // 消息弱提醒

			if (chat.fromUserId == store.state.user.operId || chat.chatType == 1) { // 发送的（当前登陆人发出去的）或者群聊的
				if (!chat.chatId) chat.chatId = chat.toUserId
				if (!chat.chatName) chat.chatName = chat.toUserName
				if (!chat.avatar) chat.avatar = chat.toUserHeadImg
			} else { // 接收的，别人发给当前登陆人的
				if (!chat.chatName) chat.chatName = chat.fromUserName
				if (!chat.chatId) chat.chatId = chat.fromUserId
				if (!chat.avatar) chat.avatar = chat.fromUserHeadImg
			}

			chat.isItMe = Boolean(chat.fromUserId == store.state.user.operId);

			// 最新一条消息
			let chatList = cache.get(cacheChatName)
			let pushTag = true;
			if (chatList && chatList.length > 0) {
				for (var i in chatList) {
					if (chatList[i].chatId == chat.chatId) {
						if (false === chat.isItMe && false === chatList[i].isOpen) {
							chat.unreadNumber = chatList[i].unreadNumber + 1
							chat.weakPrompt = true
						} else {
							chat.unreadNumber = 0
							chat.weakPrompt = false
						}

						chat.stayOnTop = Boolean(chatList[i].stayOnTop)
						chat.doNotDisturb = Boolean(chatList[i].doNotDisturb)
						chat.isOpen = Boolean(chatList[i].isOpen)
						chatList[i] = chat
						pushTag = false
					}
				}
			} else {
				chatList = [];
			}

			if (pushTag) {
				chatList.push(chat)
				if (false === chat.isItMe) increaseUserUnreadNumberTotal(chat)
			}

			// if (!chatList || chatList.length <= 0) {
			// 	chatList = [];
			// 	chatList.push(chat);
			// } else {
			// 	for (var i in chatList) {
			// 		if (chatList[i].chatId == chat.chatId) {
			// 			if (false === chat.isItMe) chat.unreadNumber = chatList[i].unreadNumber + 1
			// 			chatList[i] = chat
			// 		}
			// 	}
			// }
			cache.set(cacheChatName, chatList)
			store.commit("setConversation", chatList);

			// 所有消息
			if (chat.contentType == 5) {
				modifyPacket(chat)
			}
			addMsg(chat, chat.toUserId)
		}
	});
}

/**
 * 增加当前用户总的未读数量
 */
export const increaseUserUnreadNumberTotal = (chat) => {
	const user = cache.get("user")
	user.totalUnreadNumber += 1
	cache.set("user", user)
	store.commit("setUser", user);
}

/**
 * 减少当前用户未读总数
 * @param {object} caht 
 */
export const reduceUserUnreadNumberTotal = (chat) => {
	const user = cache.get("user")
	user.totalUnreadNumber -= chat.unreadNumber
	cache.set("user", user)
	store.commit("setUser", user);
}
