import http from '@/util/requestConfig'
import store from '@/store/index.js'
import friend from '@/util/api/friend'
import group from '@/util/api/group'
import post from '@/util/api/post'
const api = {
	init: function(){
		return new Promise((resolve, reject) => {
			let userId = store.state.user.operId;
			let req = {
				pageNum: 1,
				pageSize: 1000,
				userId
			}
			post.postList( {userId}).then(res=>{
				store.commit("setPost", res)
			});
			friend.friendList({userId}).then(res=>{
				store.commit("setFriend", res)
			});
			group.groupList( req).then(res=>{
				store.commit("setGroup", res.data)
			});
		});
	}
}
export default api