import http from '@/util/requestConfig.js';

export default {

	// 用户登录
	userLogin : function  (param) {
		return http.get('/login',param);
	},

	// 用户退出
	userLogout : function  (id) {
		return http.get('/logout/' + id,{});
	},
	
	// 用户注销
	userKickout : function  (id) {
		return http.delete('/kickout/' + id,{});
	},
	
	// 系统访问纪录
	userKickout : function  (userName) {
		return http.get('/index/' + userName,{});
	},
	
	//手机用户注册
	userRegister : function(param){
		return http.post('/register/login',param);
	},
	
	//发生注册短信
	sendRegisterSms : function(param){
		return http.post('/register/sendSms',param);
	},
	
	//找回密码
	retrievePassword : function(param){
		return http.post('/register/lookPass',param);
	},
	
	//更新用户头像
	updateUserAvatar : function(param){
		return http.post('/user/upAvatar',param,{header : {'Content-Type':'application/json'}});
	}
};