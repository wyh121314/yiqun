import http from '@/util/requestConfig.js';

export default {
	list: function(param) {
		return http.post('/app/trade/list', param);
	}
};
