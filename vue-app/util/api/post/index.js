import http from '@/util/requestConfig.js';

export default {
	// 我的群组列表
	postList: function(param) {
		return http.post('/app/post/list2', param);
	}
};
