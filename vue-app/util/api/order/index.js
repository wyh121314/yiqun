import http from '@/util/requestConfig.js';

export default {
	orderList: function(param) {
		return http.post('/order/myOrderList', param);
	}
}
