import webim from "@/utils/socket/webim.js";
import { addChat } from "@/utils/db/db.js";

/**
 * 手机号验证
 */
export const phoneFun = phone => {
  let myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
  if (!myreg.test(phone)) {
    return false;
  }
  return true;
};

// 通知群组
export const NoticeGroup = packet => {};
// 通知好友
export const NoticeFriend = packet => {};

export const NoticeMember = packet => {
  console.log(packet);
  const group = JSON.parse(packet.content);
  packet.toUserId = group.id;
  packet.toUserName = group.groupName;
  packet.toUserHeadImg = group.avatar;
  packet.chatType = 1;
  webim.joinGroup(group.id, res => {
    console.log(res);
    addChat(packet);
  });
};
