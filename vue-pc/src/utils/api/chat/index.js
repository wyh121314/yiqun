import http from "@/utils/requestConfig.js";

// 获取用户加入的群
function appGroupUserQueryGroupIds(params) {
  return http.post("/app/group/user/queryGroupIds", params);
}

// 获取普通的历史消息
function historyMsgList(params) {
  return http.post("/app/msg/list", params);
}

export default {
  appGroupUserQueryGroupIds,

  historyMsgList,

  // 发起群聊
  appGroupCreate: function(params) {
    return http.post("/app/group/create", params);
  },
  //群详情、群成员
  getGroupMember: function(params) {
    return http.post("/app/group/member", params);
  },
  //更新群名称
  upGroupName: function(params) {
    return http.post("/app/group/upGroupName", params);
  },
  //更新群自己的群昵称
  upGroupNickName: function(params) {
    return http.post("/app/group/user/upGroupNickName", params);
  },
  //群聊新增好友
  groupUserAdd: function(params) {
    return http.post("/app/group/user/add", params);
  },
  //移除群聊用户
  groupUserDel: function(params) {
    return http.post("/app/group/user/del", params);
  },
  //转让群主
  transferGroup: function(param) {
    return http.post("/app/group/transferGroup", param);
  },
  // 清空群消息
  clearGroupMsg: function(param) {
    return http.post("/app/group/msg/clearGroupMsg", param);
  },
  // 删除退出
  groupDel: function(param) {
    return http.post("/app/group/msg/clearGroupMsg", param);
  },
  // 禁止发言
  groupDis: function(param) {
    return http.post("/app/group/dis", param);
  }
};
