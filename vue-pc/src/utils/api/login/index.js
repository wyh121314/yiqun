import { post, get } from "@/utils/request";
export default {
  appGroupUserQueryGroupIds: function(params) {
    return post("/app/group/user/queryGroupIds", params);
  },
  // 用户登录
  userLogin: function(param) {
    return get("/login", param);
  },

  // 用户退出
  userLogout: function(id) {
    return get("/logout/" + id, {});
  },

  // 用户注销
  userKickout: function(id) {
    return delete ("/kickout/" + id, {});
  },

  // 系统访问纪录
  index: function(userName) {
    return get("/index/" + userName, {});
  },

  //手机用户注册
  userRegister: function(param) {
    return post("/register/login", param);
  },

  //发生注册短信
  sendRegisterSms: function(param) {
    return post("/register/sendSms", param);
  },

  //找回密码
  retrievePassword: function(param) {
    return post("/register/lookPass", param);
  },

  //更新用户头像
  updateUserAvatar: function(param) {
    return post("/user/upAvatar", param, {
      header: { "Content-Type": "application/json" }
    });
  }
};
