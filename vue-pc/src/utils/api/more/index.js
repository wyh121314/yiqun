import http from "@/utils/requestConfig.js";

export default {
  // 我的银行卡列表
  bankList: function(param) {
    return http.post("/app/bank/list", param);
  },
  // 新增银行卡
  bankAdd: function(param) {
    return http.post("/app/bank/add", param);
  },
  // 确认绑定银行卡
  bankBindCardSure: function(param) {
    return http.post("/app/bank/bindCardSure", param);
  },
  //解绑
  cancelBank: function(param) {
    return http.post("/app/bank/cancelBank", param);
  },
  //银行卡提现
  withdrawalAccount: function(param) {
    return http.post("/app/pay/withdrawalAccount", param);
  }
};
