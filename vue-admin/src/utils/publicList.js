import request from './request'

// 获取所有企业
export function getOrgKvList() {
  return request({
    url: '/system/enterprise/getOrgKvList',
    method: 'get'
  })
}

// 获取上级渠道
export function getParentKvList() {
  return request({
    url: '/system/enterprise/getParentKvList',
    method: 'get'
  })
}

// 获取所有项目
export function getProjectKvList() {
  return request({
    url: '/system/project/getProjectKvList',
    method: 'get'
  })
}
